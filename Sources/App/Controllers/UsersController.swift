import Vapor



struct UsersController: RouteCollection {

    func boot(router: Router) throws {
        let users = router.grouped("api", "users")
        users.get(User.parameter, use: getHandler)
        users.get(use: getAllHandler)
        users.post(User.self, use: createHandler)
        users.get(User.parameter, "acronyms", use: getAcronymsHandler)
    }

    func getHandler(_ req: Request) throws -> Future<User> {
        return try req.parameters.next(User.self)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[User]> {
        return User.query(on: req).all()
    }
    
    func createHandler(_ req: Request, user: User) throws -> Future<User> {
        return user.save(on: req)
    }
    
    func getAcronymsHandler(_ req: Request) throws -> Future<[Acronym]> {
        return try req
            .parameters.next(User.self)
            .flatMap(to: [Acronym].self) { user in
                return try user.acronyms.query(on: req).all()
        }
    }
    
}
